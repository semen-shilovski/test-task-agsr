package org.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    private static final String API_URL = "http://localhost:8080/patients";
    private static final String AUTH_TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkLWgySDNxY0pPZXFkWFVJQkNaUEU5MEl6MGtwS3VPcTR1TENHa0l1YlpZIn0.eyJleHAiOjE3MTgxMTYyNjEsImlhdCI6MTcxODExNDQ2MSwianRpIjoiNjViM2Q0N2QtOGI2Ny00ZWQ3LTk1YTYtYTY3NWYwZjBkYjJhIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgxL3JlYWxtcy9tZWQtcmVhbG0iLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiYzY2ODg2M2UtMWUxNi00ZDA1LWI5NzgtN2FlZjhlZTgwYWZiIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoibWVkLXNlcnZpY2UiLCJzaWQiOiIwMjg5YjAwZC1lMWM0LTRmMjYtOWJhZS1kYTYwYmFkZTYxNTIiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIi8qIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsImRlZmF1bHQtcm9sZXMtbWVkLXJlYWxtIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJtZWQtc2VydmljZSI6eyJyb2xlcyI6WyJQcmFjdGl0aW9uZXIiXX0sImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoicHJvZmlsZSBlbWFpbCBQYXRpZW50LkRlbGV0ZSBQYXRpZW50LlJlYWQgUGF0aWVudC5Xcml0ZSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYW1lIjoi0KHQtdC80ZHQvSDQqNC40LvQvtCy0YHQutC40LkiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJzZW1hIiwiZ2l2ZW5fbmFtZSI6ItCh0LXQvNGR0L0iLCJmYW1pbHlfbmFtZSI6ItCo0LjQu9C-0LLRgdC60LjQuSIsImVtYWlsIjoic2VtZW4uc2hpbG92c2tpQHlhbmRleC5ydSJ9.TZeZiZ5-WP-QGqcqhEiykz-paTtKhVoq1iZKQe-2oIjzIAskJxw5xs_mlfKjuvfgVHpSLmhff08KcB-Fm7W5ghClS1LBGPzQR6HQtF-A6CzWXSmbdxz4IfKll3lajD940WHiy1lEmHbcy8vU3nJXbeWYzIKuOZ9QDzjOMBVH-MvfVZ5rkyTgCFVitiAWcNbTkGlZAhw0CVKBVEY5yuzUrM1oP-vxQrk5b3KlJkW8niGEc5lZfZjTzsmoWoCR_WPxdrYiwEZSFLnaas7aKjBnnY1mfoOdIVoJUNZ3TZpK0FSR0KzWu0Se7UwNQYTVIaRMSOOMSLVnxTJ2z_sbtkjW5g";

    public static void main(String[] args) {
        List<CompletableFuture<Void>> futures = new ArrayList<>();
        HttpClient client = HttpClient.newHttpClient();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        for (int i = 0; i < 100; i++) {
            PatientRequest patient = generatePatient();
            try {
                String requestBody = objectMapper.writeValueAsString(patient);
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create(API_URL))
                        .header("Content-Type", "application/json")
                        .header("Authorization", "Bearer " + AUTH_TOKEN)
                        .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                        .build();

                CompletableFuture<Void> future = client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                        .thenAccept(response -> {
                            if (response.statusCode() == 200) {
                                System.out.println("Пациент успешно создан: " + response.body());
                            } else {
                                System.err.println("Ошибка при создании пациента: " + response.body());
                            }
                        });
                futures.add(future);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();
    }

    private static PatientRequest generatePatient() {
        PatientRequest patient = new PatientRequest();
        patient.setName("Patient " + UUID.randomUUID());
        patient.setGender(ThreadLocalRandom.current().nextBoolean() ? "MALE" : "FEMALE");
        patient.setBirthDate(generateRandomBirthDate());
        return patient;
    }

    private static LocalDate generateRandomBirthDate() {
        int minAge = 20;
        int maxAge = 80;
        LocalDate now = LocalDate.now();
        int randomAge = ThreadLocalRandom.current().nextInt(minAge, maxAge + 1);
        return now.minus(Period.ofYears(randomAge));
    }
}