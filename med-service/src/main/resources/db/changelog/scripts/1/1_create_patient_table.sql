create table if not exists patients
(
    id         varchar(255) primary key,
    name       varchar(255) not null,
    gender     VARCHAR(10)  not null,
    birth_date date    not null
);