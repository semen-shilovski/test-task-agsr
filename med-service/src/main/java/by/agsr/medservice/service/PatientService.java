package by.agsr.medservice.service;

import by.agsr.medservice.exception.ApiException;
import by.agsr.medservice.models.dto.PatientManipulateRequest;
import by.agsr.medservice.models.entities.Patient;
import by.agsr.medservice.repository.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class PatientService {
    private final PatientRepository patientRepository;

    public Patient getPatientById(String id) {
        return patientRepository.findById(id)
                .orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, "User with id " + id + " not found"));
    }

    public void deletePatientById(String id) {
        var patient = patientRepository.findById(id)
                .orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, "User with id " + id + " not found"));
        patientRepository.delete(patient);
    }

    public Patient createPatient(PatientManipulateRequest patient) {
        return patientRepository.save(Patient.builder()
                .name(patient.getName())
                .gender(patient.getGender())
                .birthDate(patient.getBirthDate())
                .build());
    }

    public Patient updatePatient(String id, PatientManipulateRequest updatedPatient) {
        return patientRepository.findById(id)
                .map(patient -> updateExistingPatient(patient, updatedPatient))
                .orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, "User with id " + id + " not found"));
    }

    private Patient updateExistingPatient(Patient patient, PatientManipulateRequest updatedPatient) {
        if (Objects.nonNull(updatedPatient.getName())) {
            patient.setName(updatedPatient.getName());
        }
        if (Objects.nonNull(updatedPatient.getGender())) {
            patient.setGender(updatedPatient.getGender());
        }
        if (Objects.nonNull(updatedPatient.getBirthDate())) {
            patient.setBirthDate(updatedPatient.getBirthDate());
        }
        return patientRepository.save(patient);
    }
}
