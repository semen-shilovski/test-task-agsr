package by.agsr.medservice.security.configs;

import by.agsr.medservice.security.utils.CustomJwtGrantedAuthoritiesConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig {

    private final CustomJwtGrantedAuthoritiesConverter customJwtGrantedAuthoritiesConverter;

    @Autowired
    public SecurityConfig(CustomJwtGrantedAuthoritiesConverter customJwtGrantedAuthoritiesConverter) {
        this.customJwtGrantedAuthoritiesConverter = customJwtGrantedAuthoritiesConverter;
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                /*             .authorizeHttpRequests(authorize -> authorize
                                     .requestMatchers("/patients/**").hasAnyRole("Practitioner", "Patient")
                                     .requestMatchers(HttpMethod.POST, "/patients").hasRole("Practitioner")
                                     .requestMatchers(HttpMethod.PUT, "/patients/**").hasRole("Practitioner")
                                     .requestMatchers(HttpMethod.DELETE, "/patients/**").hasRole("Practitioner")
                                     .anyRequest().permitAll()
                             )*/
                .oauth2ResourceServer(oauth2 -> oauth2.jwt(
                        jwt -> jwt.jwtAuthenticationConverter(jwtAuthenticationConverter())
                ));
        return http.build();
    }

    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter() {
        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(customJwtGrantedAuthoritiesConverter);
        return jwtAuthenticationConverter;
    }
}