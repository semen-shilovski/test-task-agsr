package by.agsr.medservice.security.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component

public class CustomJwtGrantedAuthoritiesConverter implements Converter<Jwt, Collection<GrantedAuthority>> {
    private final String keycloakResource;

    public CustomJwtGrantedAuthoritiesConverter(@Value("${keycloak.resource}") String keycloakResource) {
        this.keycloakResource = keycloakResource;
    }

    @Override
    public Collection<GrantedAuthority> convert(Jwt jwt) {
        Map<String, Object> resourceAccess = jwt.getClaim("resource_access");
        Map<String, Object> resourceAccessMap = resourceAccess != null ? (Map<String, Object>) resourceAccess.get(keycloakResource) : null;
        Collection<String> resourceRoles = resourceAccessMap != null ? (Collection<String>) resourceAccessMap.get("roles") : null;

        List<GrantedAuthority> authorities = resourceRoles != null
                ? resourceRoles.stream()
                .map(role -> new SimpleGrantedAuthority("ROLE_" + role))
                .collect(Collectors.toList())
                : new ArrayList<>();

        String scope = jwt.getClaim("scope");
        if (scope != null && !scope.isEmpty()) {
            List<GrantedAuthority> scopeAuthorities = Arrays.stream(scope.split(" "))
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
            authorities.addAll(scopeAuthorities);
        }

        return authorities;
    }
}