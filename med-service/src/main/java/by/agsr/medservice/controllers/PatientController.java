package by.agsr.medservice.controllers;

import by.agsr.medservice.models.dto.PatientManipulateRequest;
import by.agsr.medservice.models.entities.Patient;
import by.agsr.medservice.service.PatientService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/patients")
@RequiredArgsConstructor
@SecurityScheme(
        name = "tokenAuth",
        type = SecuritySchemeType.APIKEY,
        in = SecuritySchemeIn.HEADER,
        paramName = "Authorization",
        description = "Token-based Authentication"
)
public class PatientController {
    private final PatientService patientService;

    @GetMapping("/{id}")
    @Operation(
            summary = "Получить пациента по ID",
            description = "Возвращает информацию о пациенте по заданному ID",
            security = {@SecurityRequirement(name = "tokenAuth")},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Успешный запрос",
                            content = @Content(schema = @Schema(implementation = Patient.class))),
                    @ApiResponse(responseCode = "404", description = "Пациент не найден")
            }
    )
    @PreAuthorize("hasAnyRole('ROLE_Practitioner', 'ROLE_Patient') and hasAuthority('Patient.Read')")
    public ResponseEntity<Patient> getPatientById(@PathVariable String id) {
        return ok(patientService.getPatientById(id));
    }

    @PreAuthorize("hasRole('ROLE_Practitioner') and hasAuthority('Patient.Write')")
    @Operation(
            summary = "Создать нового пациента",
            description = "Создает нового пациента с предоставленными данными",
            security = {@SecurityRequirement(name = "tokenAuth")},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Пациент успешно создан",
                            content = @Content(schema = @Schema(implementation = Patient.class))),
                    @ApiResponse(responseCode = "400", description = "Неверные данные")
            }
    )
    @PostMapping
    public ResponseEntity<Patient> createPatient(@RequestBody @Valid PatientManipulateRequest patient) {
        return ok(patientService.createPatient(patient));
    }

    @PreAuthorize("hasRole('ROLE_Practitioner') and hasAuthority('Patient.Write')")
    @Operation(
            summary = "Обновить информацию о пациенте",
            description = "Обновляет данные существующего пациента по ID",
            security = {@SecurityRequirement(name = "tokenAuth")},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Пациент успешно обновлен",
                            content = @Content(schema = @Schema(implementation = Patient.class))),
                    @ApiResponse(responseCode = "404", description = "Пациент не найден")
            }
    )
    @PutMapping("/{id}")
    public ResponseEntity<Patient> updatePatient(@PathVariable String id, @RequestBody PatientManipulateRequest patient) {
        return ok(patientService.updatePatient(id, patient));
    }

    @PreAuthorize("hasRole('ROLE_Practitioner') and hasAuthority('Patient.Delete')")
    @Operation(
            summary = "Удалить пациента",
            description = "Удаляет пациента по заданному ID",
            security = {@SecurityRequirement(name = "tokenAuth")},
            responses = {
                    @ApiResponse(responseCode = "204", description = "Пациент успешно удален"),
                    @ApiResponse(responseCode = "404", description = "Пациент не найден")
            }
    )
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePatient(@PathVariable String id) {
        patientService.deletePatientById(id);
    }
}