package by.agsr.medservice.models.dto;

import by.agsr.medservice.models.entities.Patient;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientManipulateRequest {
    @NotNull
    @Schema(description = "Имя пациента", example = "John Doe")
    private String name;
    @NotNull
    @Schema(description = "Пол пациента", example = "MALE")
    private Patient.Gender gender;
    @NotNull
    @Schema(description = "Дата рождения пациента", example = "2000-01-01")
    private LocalDate birthDate;
}
