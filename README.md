# Java Тестовое задание AGSR

## Варианты запуска

1. Запуск всех контейнеров из одного docker compose, используя образ разработанного API из DockerHub
Тогда следует выполнить следующие шаги из части "Настройка и запуск"
Пункт 3 -> Пункт 2
2. Запуск контейнеров Keycloack, БД для Keycloack, БД для сервера
Тогда следует выполнить следующие шаги из части "Настройка и запуск"
Пункт 1 -> Пункт 2 ->  Пункт 4

## Настройка и запуск


1. Запуск контейнеров для локальной разработки (Keycloack, Psql Keycloack, Psql Java)
* Из корня проекта перейти в пакет local-containers
* Выполнить команду `docker-compose up`

2. Настройка Keycloack
* Создать realm `med-realm`
* Добавить клиента `med-service` с возможностью авторизации
* Добавить клиенту роли `Patient` и `Practitioner`
* Добавить скоупы `Patient.Read`, `Patient.Write`, `Patient.Delete`; В настройках установить положительный флаг для `Include in token scope`
* Для скоупа `Patient.Read` добавить роль `Patient`, `Practitioner`; Для остальных созданных скоупов - только роль `Practitioner`.
Пример :
![img_2.png](img_2.png)
* Добавить юзера с ролью `Patient`;
* Добавить юзера с ролью `Practitioner`; 
Получение токенов :
```curl
curl --location 'http://localhost:8081/realms/med-realm/protocol/openid-connect/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--header 'Cookie: JSESSIONID=73D56C10A84752361438DA653226D10D' \
--data-urlencode 'client_id=med-service' \
--data-urlencode 'username=user' \
--data-urlencode 'password=123' \
--data-urlencode 'grant_type=password' \
--data-urlencode 'client_secret=aORg45YTMWvGHb2bgdynd6Sp4ZazTubo'
```

3. Запуск API с DockerHub, совместно с контейнерами
* Из корня проекта перейти в пакет local-containers-with-java-api
* Выполнить команду `docker-compose up`

4. Настройка API, с использованием среды разработки
* Из корня проекта открыть приложение med-service
* Запустить проект с dev профилем : `./gradlew bootRun --args='--spring.profiles.active=dev'`[README.md](med-service%2FREADME.md)